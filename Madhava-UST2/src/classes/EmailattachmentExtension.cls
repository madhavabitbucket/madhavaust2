public class EmailattachmentExtension {
public boolean bEmailPreviewPanel{set;get;}
public string struseremail{set;get;}
public String strSelectedOppPDFName {get;set;}//Gettter-Setter to capture Opportunity related PDF document Name
    
public Quote quoteObj{get;set;}//Local ariable to capture Quote information
public string toMailAddressesString{set;get;}
private Map<Id,Id> attachmentOppRemoveMap=new Map<Id,Id>();
     public List<Attachment> attachmentOppList = new List<Attachment>();//List of Opportunity related Attachements
   
 public Id idSelectedOppPDF {get;set;}//Gettter-Setter to capture Opportunity related PDF document

public List<Document> listExtraAttachments = new List<Document>();//to store extra attachments to the mail

public List<Attachment> attachmentOppSelectedList{get;set;}
   public Map<Id,Attachment> attachmentOppMap = new Map<Id,Attachment>();//Map of Opportunity related Attachements

    public EmailattachmentExtension(ApexPages.StandardController controller) {
struseremail='madhavaust@gmail.com';
tomailaddressesstring='madhava.ustglobal@gmail.com';
attachmentOppSelectedList = new List<Attachment>();
 quoteObj = (Quote) controller.getRecord();
 quoteObj = [select Id, Name, opportunityId,ExpirationDate,Opportunity.Name,Opportunity.CreatedById, Opportunity.stageName, Opportunity.AccountId, Opportunity.Account.Name,
                    opportunity.Account.Parent.Name,opportunity.Account.Type
                        from Quote where Id=:quoteObj.Id LIMIT 1]; 
 
 List<Opportunity_Attachment__c> oppAttachmentList=[select id from Opportunity_Attachment__c where Opportunity__c=:quoteObj.Opportunity.id];
  attachmentOppList = [select id, Name, parentId, LastModifiedDate From Attachment where parentId IN:oppAttachmentList order by LastModifiedDate desc LIMIT 999];
       
    if(attachmentOppList.size()!=0)
    {
        for(Integer i=0;i<attachmentOppList.size();i++){
            attachmentOppMap.put(attachmentOppList[i].Id , attachmentOppList[i]);        
            idSelectedOppPDF=attachmentOppList[0].Id;
            strSelectedOppPDFName=attachmentOppList[0].Name;
        }
    }
    }
   
    
 public PageReference addOppAttachments(){
        if(idSelectedOppPDF!=null){
            attachmentOppSelectedList.add(attachmentOppMap.get(idSelectedOppPDF));
            attachmentOppRemoveMap.put(idSelectedOppPDF,idSelectedOppPDF);
        }
        return null;
    }
public Decimal dSeqAttachOpp{get;set;}
public ApexPages.PageReference removeExtraAttachmentOpp(){
        Id idRemoved=attachmentOppSelectedList.remove(dSeqAttachOpp.intValue()-1).id;
        attachmentOppRemoveMap.remove(idRemoved);
        return null;
    }
    public List<SelectOption> getOppRecordPDFOptions(){
        List<SelectOption> options = new List<SelectOption>();
        for(Attachment attachment : attachmentOppMap.values()){
            if(attachmentOppRemoveMap.isEmpty()){
                options.add(new selectoption(attachment.Id, attachment.Name));
            }else{
                if(!attachmentOppRemoveMap.containsKey(attachment.Id))
                    options.add(new selectoption(attachment.Id, attachment.Name));
            }
            
        }
    
    return options; 
}}