public class AccountSearchController {
    public String selectedAccName {set;get;}
    public String selectedZip {set;get;}
    public String qry {set;get;}
    public List<innerCls> lstAcc {set;get;}
    static Integer globalCounter;
   
    public AccountSearchController (){
        lstAcc = new List<innerCls>();
        globalCounter = 0;
    }
    public void searchAcc(){
        globalCounter = 1;
        lstAcc = new List<innerCls>();
        String qry = ' SELECT Id,Name,BillingStreet, BillingState,BillingPostalCode,BillingCountry, BillingCity From Account';

          for(Account acc:Database.query(qry))
          {
                lstAcc.add(new innerCls(acc));
                }
    }
    public class innerCls{
        public Integer count{set;get;}
        public Account acc {set;get;}
        public innerCls(Account acc){
                this.count = globalCounter++;
                this.acc = acc;
        }
    }
}