@isTest
global class implementationrestclass implements HttpCalloutMock {
    // Implement respond() interface method
    
    global HTTPResponse respond(HTTPRequest req) {
        System.assertEquals('http://www.cheenath.com/tutorial/sample1/build.xml', req.GETEndpoint());
        System.assertEquals('GET', req.getMethod());
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.SETHeader('Content-Type', 'text/xml');
           res.SETBody('<env:Envelope><env:Header></env:Header><env:Body><ghr:return><ghr:array><ghr:item>'+
         '<ghr:account>PT015AC</ghr:account><ghr:key>PT015AC</ghr:key><ghr:lmMon>201205</ghr:lmMon><ghr:dspMon>May 2012</ghr:dspMon>'+
         '<ghr:total>0.00</ghr:total><ghr:totTrade>0.00</ghr:totTrade><ghr:status/></ghr:item>'+
         '</ghr:array></ghr:return></env:Body></env:Envelope>');
            res.SETStatusCode(200);
        return res;
    }
}