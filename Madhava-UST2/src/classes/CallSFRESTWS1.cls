public class CallSFRESTWS1 {    
    public CallSFRESTWS1(){    
    }    
    public string responseFromWs {get;set;}   
    public string response1 {get;set;}
    public string response2 {get;set;}
    public string response3 {get;set;}
    public string response4 {get;set;}
    public string response5 {get;set;}
    
   // public void findResponseFromRestofOrg1(){     
   public void findResponse(){
      /*   ***********NOTES******** 
      1)www.salesforce.com is only for making login requests, 
      2)if it was to a sandbox,
         use :request.setEndpoint('https://test.salesforce.com/services/Soap/u/22.0');
      3)your REST API requests should goto the specifc instance e.g. ap1-api.salesforce.com
      */   
      
      /*  
      ***********************************************************************
           1.  POST username and password via REST API to get SESSION ID
           Request = POST / XML
      ***********************************************************************
      */
        String username = 'madhavaust@gmail.com';          
        String password = 'cloudcomputing1';
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://www.salesforce.com/services/Soap/u/22.0');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' +username+ '</username><password>' +password+ '</password></login></Body></Envelope>');
        Http h = new Http();
        HttpResponse res = h.send(request);// returns XML
        response1 = res.getBody();
                         /* 1. XML response Body
                          1.RES = 
                          <soapenv:Body>
                          <loginResponse>
                             <result>
                              <metadataServerUrl>https://ap1-api.salesforce.com/services/Soap/m/22.0/00D90000000lqcP</metadataServerUrl>
                              <sandbox>false</sandbox>
                              <serverUrl>https://ap1-api.salesforce.com/services/Soap/u/22.0/00D90000000lqcP</serverUrl>
                              <sessionId>00D90000000lqcP!AQIAQPpAO5BzvO25dcSiHN4fhYHzY1c8EkJ_HzYNEbtHR7_wBQndoxvqsQwnXfVlSgGNc6JUE_JCQGW.RdbsjwqbB7wX1HSq</sessionId>
                              <userId>00590000001e4IL</userId>
                              <userInfo>
                                  <organizationId>00D90000000lqcP</organizationId>
                                  <organizationName>ust</organizationName>
                                  <profileId>00e900000019JTv</profileId>
                                  <roleId xsi:nil="true"/>
                                  <sessionSecondsValid>7200</sessionSecondsValid>
                                  <userEmail>madhavaust@gmail.com</userEmail>
                                  <userFullName>madhav ust</userFullName>
                                  <userId>00590000001e4IL</userId
                                  <userName>madhavaust@gmail.com</userName>
                                </userInfo>           
                              </result>
                          </loginResponse>
                          </soapenv:Body>
                          </soapenv:Envelope> */
        Dom.XmlNode resultElmt  = res.getBodyDocument().getRootElement().getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/').getChildElement('loginResponse','urn:partner.soap.sforce.com').getChildElement('result','urn:partner.soap.sforce.com');
                        /* 1.  XML response body in XML DOM Parser
                                    @@resultElmt  ==XMLNode[ELEMENT,result,urn:partner.soap.sforce.com,null,null,
                                         [XMLNode[ELEMENT,metadataServerUrl,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,https://ap1-api.salesforce.com/services/Soap/m/22.0/00D90000000lqcP,]],null,], 
                                            XMLNode[ELEMENT,passwordExpired,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,false,]],null,], 
                                            XMLNode[ELEMENT,sandbox,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,false,]],null,], 
                                            XMLNode[ELEMENT,serverUrl,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,https://ap1-api.salesforce.com/services/Soap/u/22.0/00D90000000lqcP,]],null,], 
                                            XMLNode[ELEMENT,sessionId,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,00D90000000lqcP!AQIAQA_Dcd4X8fIt9ujtPZGPRj1595PW8jwuwhHlW_icT3SP4tqJ6e3t3WBrcJWDhlgDvQum6vlS7c2BCV.e6D6xK.Tyta8h,]],null,], 
                                            XMLNode[ELEMENT,userId,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,00590000001e4IL,]],null,], 
                                            XMLNode[ELEMENT,userInfo,urn:partner.soap.sforce.com,null,null,
                                                 [XMLNode[ELEMENT,accessibilityMode,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,false,]],null,], 
                                                  XMLNode[ELEMENT,organizationId,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,00D90000000lqcP,]],null,], 
                                                  XMLNode[ELEMENT,organizationMultiCurrency,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,false,]],null,], 
                                                  XMLNode[ELEMENT,organizationName,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,UST Global Inc,]],null,], 
                                                  XMLNode[ELEMENT,profileId,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,00e900000019JTv,]],null,], 
                                                  XMLNode[ELEMENT,roleId,urn:partner.soap.sforce.com,[common.apex.api.dom.XmlNode$Attribute@1c70325c],[common.apex.api.dom.XmlNode$NamespaceDef@9fa449a],null,null,], 
                                                  XMLNode[ELEMENT,sessionSecondsValid,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,7200,]],null,], 
                                                  XMLNode[ELEMENT,userEmail,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,madhavaust@gmail.com,]],null,], 
                                                  XMLNode[ELEMENT,userFullName,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,madhav ust,]],null,], 
                                                  XMLNode[ELEMENT,userId,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,00590000001e4IL,]],null,], 
                                                  XMLNode[ELEMENT,userName,urn:partner.soap.sforce.com,null,null,[XMLNode[TEXT,null,null,null,null,null,madhavaust@gmail.com,]],null,],    */
        // Grab session id and server url (ie the session)      
        final String SERVER_URL = resultElmt.getChildElement('serverUrl','urn:partner.soap.sforce.com').getText().split('/services')[0];
        final String SESSION_ID = resultElmt.getChildElement('sessionId','urn:partner.soap.sforce.com').getText(); 
                                           /*@@ server url ==https://ap1-api.salesforce.com*/
       
        /*
        ***********************************************************************************************
           2.  POST Account Details to org1 to create an Account in org1 using REST custom web service 
            Request = POST / json
       ***********************************************************************************************
       */
       //Calling custom REST web service - @RestResource(urlMapping='/showAccounts/*')
        final PageReference theUrl4 = new PageReference(SERVER_URL + '/services/apexrest/showAccounts/');
        request = new HttpRequest();
        request.setEndpoint(theUrl4.getUrl());
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'OAuth ' + SESSION_ID); 
        request.setHeader('SOAPAction','MyRestResource_sidd');
        request.setBody('{"name":"GenePoint2","phone":"95620626","website":"www.google.com"}');
        HttpResponse respnse = (new Http()).send(request);//HttpRequest[Endpoint=https://ap1-api.salesforce.com/services/apexrest/showAccounts/, Method=POST]
        response4 = respnse.getBody();
        /*  
        *************************************************************************************************
           3.  POST Account Details to org1 to create an Account in org1 using REST custom web service 
            Request = POST / XML
          *************************************************************************************************
       */
       //Calling custom REST web service - @RestResource(urlMapping='/showAccounts/*')
        final PageReference theUrl5 = new PageReference(SERVER_URL + '/services/apexrest/showAccounts/');
        request = new HttpRequest();
        request.setEndpoint(theUrl5.getUrl());
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/xml');
        request.setHeader('Authorization', 'OAuth ' + SESSION_ID); 
        request.setHeader('SOAPAction','MyRestResource_sidd');
            request.setBody('<request>'+
            '<name>Org2AccntName1</name><phone>95620626</phone><website>www.google.com</website>'+
            '</request>');
        HttpResponse respons = (new Http()).send(request);//HttpRequest[Endpoint=https://ap1-api.salesforce.com/services/apexrest/showAccounts/, Method=POST]
        response5 = respons.getBody();
        }
}