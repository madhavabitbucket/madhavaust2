/**
* This class is a webservice that returns information from the MyObject__c level
*/
global with sharing class MyObjectAPI {
    /**
    * This class is passed in from the client to specify the calling context
    */
    global class AccountContext {
        WebService String accountNumber;
 
        /**
        * A blank constructor
        */
        public AccountContext() {}
 
        /**
        * A constructor based on an account
        * @param a An account
        */
        public AccountContext(Account a) {
            this.accountNumber = a.AccountNumber;
        }
    }
 
    /**
    * Gets all related MyObjects for a given account
    * @param context The account context
    * @return The related MyObjects
    */
    
}