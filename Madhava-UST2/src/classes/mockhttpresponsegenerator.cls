@istest
global class mockhttpresponsegenerator implements httpcalloutmock{
global httpresponse respond(httprequest req){
system.assertequals('http://cheenath.com/tutorial/sample1/build.xml',req.getendpoint());
system.assertequals('get',req.getmethod());
httpresponse res=new httpresponse();
res.setheader('content-type','text/xml');
res.setBody('<env:Envelope><env:Header></env:Header><env:Body><ghr:return><ghr:array><ghr:item>'+
         '<ghr:account>PT015AC</ghr:account><ghr:key>PT015AC</ghr:key><ghr:lmMon>201205</ghr:lmMon><ghr:dspMon>May 2012</ghr:dspMon>'+
         '<ghr:total>0.00</ghr:total><ghr:totTrade>0.00</ghr:totTrade><ghr:status/></ghr:item>'+
         '</ghr:array></ghr:return></env:Body></env:Envelope>');
        res.setStatusCode(200);

return res;
}
}