@isTest
private class TestCallExternalSOAPbasedWS {

    @isTest static void testShakesphereSOAPCallOut() {
        //second argument - pass a new instance of your interface implementation of WebServiceMock
       Test.setMock(WebServiceMock.class, new MockSOAPWebServiceResponseGenerator());
       sakesperesampleclass contr = new sakesperesampleclass();
       contr.wordtosearchinexternalws= 'kingdom';
       contr.getshakespearespeach();      
       System.assertEquals('<SPEECH><PLAY>MACBETH</PLAY><SPEAKER>MACDUFF</SPEAKER>Hail, king! for so thou art: behold, where stands The usurpers cursed head: the time is free: I see thee compassd with thy kingdoms pearl, That speak my salutation in their minds; Whose voices I desire aloud with mine: Hail, King of Scotland!</SPEECH>', contr.responsefromexternalws);
    }
}