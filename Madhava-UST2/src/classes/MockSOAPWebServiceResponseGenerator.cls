@isTest
global class MockSOAPWebServiceResponseGenerator implements WebServiceMock{
    //specify the fake response in the doInvoke method of WebServiceMock class
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,   
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {
        sakesperewebserviceclass.GetSpeechResponse_element respElement = new sakesperewebserviceclass.GetSpeechResponse_element();
        // Populate response element
        respElement.GetSpeechResult = '<SPEECH><PLAY>MACBETH</PLAY><SPEAKER>MACDUFF</SPEAKER>Hail, king! for so thou art: behold, where stands The usurpers cursed head: the time is free: I see thee compassd with thy kingdoms pearl, That speak my salutation in their minds; Whose voices I desire aloud with mine: Hail, King of Scotland!</SPEECH>';
        // Add response element to the response parameter
        response.put('response_x', respElement);
    }
}