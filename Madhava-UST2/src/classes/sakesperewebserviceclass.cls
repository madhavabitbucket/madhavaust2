//Generated by wsdl2apex

public class sakesperewebserviceclass {
    public class GetSpeech_element {
        public String Request;
        private String[] Request_type_info = new String[]{'Request','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlme.com/WebServices','true','false'};
        private String[] field_order_type_info = new String[]{'Request'};
    }
    public class GetSpeechResponse_element {
        public String GetSpeechResult;
        private String[] GetSpeechResult_type_info = new String[]{'GetSpeechResult','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlme.com/WebServices','true','false'};
        private String[] field_order_type_info = new String[]{'GetSpeechResult'};
    }
    public class ShakespeareSoap {
        public String endpoint_x = 'http://www.xmlme.com/WSShakespeare.asmx';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://xmlme.com/WebServices', 'sakesperewebserviceclass'};
        public String GetSpeech(String Request) {
            sakesperewebserviceclass.GetSpeech_element request_x = new sakesperewebserviceclass.GetSpeech_element();
            sakesperewebserviceclass.GetSpeechResponse_element response_x;
            request_x.Request = Request;
            Map<String, sakesperewebserviceclass.GetSpeechResponse_element> response_map_x = new Map<String, sakesperewebserviceclass.GetSpeechResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://xmlme.com/WebServices/GetSpeech',
              'http://xmlme.com/WebServices',
              'GetSpeech',
              'http://xmlme.com/WebServices',
              'GetSpeechResponse',
              'sakesperewebserviceclass.GetSpeechResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.GetSpeechResult;
        }
    }
}