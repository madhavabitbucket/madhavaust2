global class autoCompleteController {
    @RemoteAction
    global static SObject[] findSObjects(string obj, string Cargo_Type, string Trade_Type, string qry) {
        System.Debug('=======>>obj'+obj);
        System.Debug('=======>>Cargo_Type'+Cargo_Type);
        System.Debug('=======>>Trade_Type'+Trade_Type);
        System.Debug('=======>>qry'+qry);        
        //Create the filter text
        String soql = '';
        if(obj == 'Commodities_Map__c')
        {
            String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
            //begin building the dynamic soql query
            soql = 'select Commodity_Name__c, HS_Code_MARS__c, Parent_Commodity_Code__c, Is_Parent_Commodity__c';
            // add the object and filter by name to the soql
            soql += ' from Commodities_Map__c where ( Commodity_Name__c' + filter + ' or HS_Code_MARS__c ' + filter;
            //Check for Opportunity Cargo Type
            if(Cargo_Type == 'DRY')
            {
                soql += ') AND ( DRY_Commodity__c = TRUE OR Both_DRY_and_REEF__c = TRUE )';
            }
            else
            {
                soql += ') AND ( REEF_Commodity__c = TRUE OR Both_DRY_and_REEF__c = TRUE )';
            }
            soql += ' order by Parent_Commodity_Code__c limit 100';
        }
        /*Its for Users*/
        else if(obj == 'User')
        {
            String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
            //begin building the dynamic soql query
            soql = 'select id, FirstName, LastName, Name, Username';
            // add the object and filter by name to the soql
            soql += ' from User where ( FirstName' + filter + ' or LastName ' + filter + ' or Name ' + filter + ' )';
            soql += ' and ProfileId in (Select ID from Profile where Name = \'GSC\') order by Name limit 20';
        }
        /*Its For Geography*/
        else if(obj == 'Geography__c')
        {
            String filter = ' like \'' + qry + '%\'';
            if (Trade_Type == 'CY')
            {
                soql = 'select id, name, Port_City__c, Type_Of_Location__c , Parent_Geography__r.Name, Geography_With_Country_Code__c from Geography__c';
                soql += ' where ( Port_City__c' + filter + ' or GEO_Code__c ' + filter + ' ) and ((Type_of_Location__c = \'City\') or (Type_of_Location__c = \'Country\') or (Type_of_Location__c = \'State\') or (Type_of_Location__c = \'Province\')) limit 20';
            }
            else
            {
                soql = 'select id, name, Port_City__c, Type_Of_Location__c , Parent_Geography__r.Name, Geography_With_Country_Code__c from Geography__c';
                soql += ' where ( Port_City__c' + filter + ' or GEO_Code__c ' + filter + ' ) and (Type_of_Location__c = \'City\') limit 20';
            }
        }
        
        system.debug('Expected soql Query : '+ soql);
        
        List<sObject> L = new List<sObject>();
        try {
            L = Database.query(soql);
        }
        catch (QueryException e) {
            return null;
        }
        return L;
   }
}