public class accountwraperlist
{
    public accountwraperlist(ApexPages.StandardController controller) {
data();

    }


list<rate__C> Ratelist=new list<Rate__c>();
map<id,Rate__c> ratemapval=new map<id,rate__C>();
public void data()
{
if(wplist.size()>0)
wplist.clear();

Ratelist=[Select id,name,PnQ_Err_Code__c,PnQ_effective_until_date__c, isBCO__c,Commodity__c,Container_Size__c,Container_Type__c,Origin_Trade_Type__c, Destination_Trade_Type__c, 
                                            Weight__c,ReceiptLocation__r.GEO_ID__c,DeliveryLocation__r.GEO_ID__c, hazardous__c,                                             DeliveryLocation__r.Geography_With_Country_Code__c, DeliveryLocation__r.Name,ReceiptLocation__r.name from Rate__c];
for(Rate__C r:Ratelist)
{
ratemapval.put(r.id,r);
wraperclass w=new wraperclass();
w.id=r.id;
w.name=r.name;

w.commodity=r.Commodity__c;
wplist.add(w);


}

}
public void del()
{
Rate__c rcc;
for(wraperclass w:wplist)
{
if(w.checkval==true)
{
rcc=ratemapval.get(w.id);
//acc=[select id from account where id=w.id];
}
delete rcc; 

}


data();
}

public void deleteall()
{
list<Rate__C> rccdellist=new list<Rate__C>();
for(wraperclass w:wplist)
{
if(w.checkval==true)
{
Rate__C rcc=ratemapval.get(w.id);
rccdellist.add(rcc);
}

}
delete rccdellist;
data();

}
list<wraperclass> wplist=new list<wraperclass>();
public class wraperclass
{
public id id{set;get;}
public string name{set;get;}
public string commodity{set;get;}
public boolean checkval{set;get;}

}
public list<wraperclass> getwplist()
{

return wplist;
}

public void selectall()
{
for(wraperclass w:wplist)
{
w.checkval=true;

}

}
public void deselectall()
{
for(wraperclass w:wplist)
{
w.checkval=false;

}

}



}