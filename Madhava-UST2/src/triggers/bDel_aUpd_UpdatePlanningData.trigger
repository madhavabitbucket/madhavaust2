/*
    This Trigger will Update all Standard Event fields on corresponding Planning Data record.
    If Event is deleted then respective Planning data will also be deleted.
*/    

trigger bDel_aUpd_UpdatePlanningData on Event (after update , before Delete) 
{

    set<ID> EventIdsSet = new set<ID>();
    Map<ID, Event> IdEventMap = New Map<ID, Event>();
    Set<ID> ConIdsSet = New Set<ID>();
    Set<ID> LeadIdsSet = New Set<ID>();    
    Map<ID, Contact> IdContactMap = New Map<ID, Contact>();
    Map<ID, Lead> IdLeadMap = New Map<ID, Lead>();    
    Event eve;
    
    if( Trigger.IsDelete )
    {
        For( Event e : Trigger.Old )
        {
            EventIdsSet.add(e.Id);
        }
        
        List<Planning_Data__c> planningdatalist = [ Select Id from Planning_data__c where Activity_ID__c IN: EventIdsSet ];

        if( planningdatalist.size()>0 )
            Delete planningdatalist;    
        
    }
    else if( Trigger.IsUpdate )
    {
        for (Event e: trigger.new)
        {
            EventIdsSet.add(e.Id);
        }
        
        for( Event e : [ Select id,Description,Type,location,StartDateTime,EndDateTime,DurationInMinutes,IsAllDayEvent,
                        WhoId,WhatId,Who.Name,What.Name,Who.Type,What.Type,Who.Email,Who.Phone,AccountId,OwnerId,
                        IsPrivate,ShowAs,ActivityDateTime,Subject,Executive_Selling__c,
                        Internal_Notes__c,Pre_Call_Notes__c,Accompanied_by_Sales_Manager__c,Post_call_Notes__c from Event where id = : EventIdsSet ] )
        {
            IdEventMap.put(e.id, e);  
    
            if( e.Who.Type == 'Contact' )
                ConIdsSet.add(e.Whoid);
            else if( e.Who.Type == 'Lead' )
                LeadIdsSet.add(e.Whoid);       
        }
          
        if( ConIdsSet.size()> 0 )
        {
            for( Contact c : [ Select id,Email,Phone from Contact where ID IN: ConIdsSet ] )
            {
                IdContactMap.put(c.id, c);
            }
        }
    
        if( LeadIdsSet.size()> 0 )
        {
            for( Lead l : [ Select id,Email,Phone from Lead where ID IN: LeadIdsSet ] )
            {
                IdLeadMap.put(l.id, l);
            }
        }
          
        if(IdEventMap.keySet().size() > 0)
        {
            List<Planning_Data__c> PlannigDataList = [select ID, Activity_Subject__c, Activity_AllDayEvent__c , Activity_Private__c , Activity_Show_Time_As__c , Activity_Time__c, Activity_Start_Date__c, 
                                                    Activity_Duration__c, Activity_Email__c, Activity_End_Date__c,Activity_ID__c,Activity_Phone__c,Activity_Type__c,Activity_What_ID__c,Activity_Who_ID__c,  
                                                    Activity_Related_To_Name__c, Activity_Who_ID_Type__c, Activity_What_ID_Type__c, Agenda__c, Activity_Location__c, Activity_Who_Name__c, Associated_Account__c,
                                                    Accompanied_by_Sales_Manager__c, Executive_Selling__c, Activity_Assigned_To__c from Planning_Data__c where Activity_ID__c in : IdEventMap.keySet() ];

            if(PlannigDataList.size() > 0)
            {
                for(Planning_Data__c plan : PlannigDataList )
                {
                    eve = IdEventMap.get( plan.Activity_ID__c );
                    if( eve != NULL )
                    {              
                        plan.Activity_Subject__c = eve.Subject;
                        plan.Activity_Start_Date__c = eve.StartDateTime;
                        plan.Activity_End_Date__c = eve.EndDateTime;
                        plan.Activity_Time__c = eve.ActivityDateTime;
                        plan.Activity_Duration__c = eve.DurationInMinutes;
                        
                       plan.Accompanied_by_Sales_Manager__c = eve.Accompanied_by_Sales_Manager__c;                
                        plan.Executive_Selling__c = eve.Executive_Selling__c;                
                        plan.Associated_Account__c = eve.AccountId; 
                        plan.Ownerid = eve.ownerid;
                        plan.Activity_Assigned_To__c = eve.OwnerId;                
                        
                        plan.Activity_Type__c = eve.Type;   
                        plan.Activity_Location__c = eve.Location;   

                        plan.Activity_AllDayEvent__c = eve.IsAllDayEvent;
                        plan.Activity_Private__c = eve.IsPrivate;
                        plan.Activity_Show_Time_As__c = eve.ShowAs;
                        
                        plan.Activity_What_ID__c = eve.WhatId;
                        plan.Activity_Who_ID__c = eve.WhoId;                                         
                        plan.Activity_Who_Name__c = eve.Who.Name ;            
                        plan.Activity_Related_To_Name__c = eve.What.Name ;
                        plan.Activity_Who_ID_Type__c =  eve.Who.Type ;
                        plan.Activity_What_ID_Type__c = eve.What.Type ;
                             
                        if( plan.Agenda__c != eve.Description )
                            plan.Agenda__c = eve.Description ;                            

                        if( eve.Who.Type == 'Contact' && IdContactMap.get(eve.WhoId) != NULL )
                        {
                            plan.Activity_Email__c = IdContactMap.get(eve.WhoId).Email ;                
                            plan.Activity_Phone__c = IdContactMap.get(eve.WhoId).Phone ;                                                                       
                        }
                        else if( eve.Who.Type == 'Lead' && IdLeadMap.get(eve.WhoId) != NULL )
                        {
                            plan.Activity_Email__c = IdLeadMap.get(eve.WhoId).Email ;                
                            plan.Activity_Phone__c = IdLeadMap.get(eve.WhoId).Phone ;                                               
                        }                              
                    }
                }   
                             
                // Update change to the accounts
                update PlannigDataList;
            }                        
        }
    }
}